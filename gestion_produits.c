
#include "gestion_produits.h"
#include "/usr/local/Cellar/mysql-client/8.0.18/include/mysql/mysql.h"
#include <mysql.h>

/**
Fonction de saisie d'un produit
*/
Prod saise_new_prod()
{
    Prod p;
    printf("introduire le nom du produit :");
    scanf("%s",p.nom);
    p.nom[40]='\0'; // on ajouter \0 pour la fin de la chaine de carat�re
    //Saisie de la deuxi�me partie du produit : le prix
    printf("introduire le prix du produit :");
    scanf("%f",&p.prix);

    return p;
}

/***
Fonction de saisie de la liste de produit
*/
void saisie_liste_produit(int nb, Prod TABPROD[])
{
    int i;

    for(i=0; i<nb; i++)
    {
        //Saisie de la premi�re partie du produit : le nom
        printf("introduire le nom du produit :");
        scanf("%s",TABPROD[i].nom);
        TABPROD[i].nom[40]='\0'; // on ajouter \0 pour la fin de la chaine de carat�re
        //Saisie de la deuxi�me partie du produit : le prix
        printf("introduire le prix du produit :");
        scanf("%f",&TABPROD[i].prix);
    }
}
/**
Fonction d'affichage d'un tableau de structure
*/
void affiche_liste_produit(int nb, Prod TABPROD[])
{
    int i;
    for(i=0; i<nb; i++)
    {
        printf("|%d|%s|%f|\n",i+1,TABPROD[i].nom,TABPROD[i].prix);
    }
}
/**
Fonction de Permutation de deux variable de type structure
*/
void permuter(Prod *A, Prod *B)
{
    Prod AIDE;

    AIDE.prix = A->prix;
    strcpy(AIDE.nom, A->nom);

    A->prix = B->prix;
    strcpy(A->nom, B->nom);

    B->prix = AIDE.prix;
    strcpy(B->nom, AIDE.nom);
}
/***
Fonction de tri de tableau de structure par prix
*/
void tri_liste_produit_prix(int N, Prod TABPROD[])
{
    int i;
    int j;
    /********************************************************/
    /**Trie � Bull du tableau**/
    /********************************************************/
    for (i=0; i<N-1; i++)
    {
        for (j=i+1; j<N; j++)
        {
            if (TABPROD[j].prix>TABPROD[i].prix)
            {
                permuter(&TABPROD[j],&TABPROD[i]);
            }
        }
    }
}

/***
Fonction de Tri d'un tableau de structure par nom
*/
void tri_liste_produit_nom(int N, Prod TABPROD[])
{
    int i;
    int j;
    for (i=0; i<N-1; i++)
    {
        for (j=i+1; j<N; j++)
        {
            if (strcmp(TABPROD[j].nom,TABPROD[i].nom)>0)
            {
                permuter(&TABPROD[j],&TABPROD[i]);
            }
        }
    }
}
/**
Retourne 1 ou 0 selon le resultat de l'insertion
*/
int insert_nouveau_produit(int nb, Prod TABPROD[], Prod produitToInert)
{
    int res = 0; // le r�sultat est par d�faut = 0

    if(nb<MAX_SIZE_TAB)  // si le tableau n'est pas compl�tement remplis
    {
        TABPROD[nb].prix= produitToInert.prix; // on insert le produit � la fin du tableau
        strcpy(TABPROD[nb].nom, produitToInert.nom); // on insert le produit � la fin du tableau
        res = 1; // l'insertion s'est bien pass�e du coup on renvoi 1
    }
    return (res);
}


/**
Fonction qui retourne la posiion dans le tableau oubien -1
*/
int recherche_produit_into_list_produit(int N, Prod TABDPROD[], Prod produitToFind)
{
    int i;
    int POS;

    /**Recherche de la valeur */
    for(i=0, POS = -1; i<N; i++)
    {
        if(TABDPROD[i].prix==produitToFind.prix && !strcmp(TABDPROD[i].nom,produitToFind.nom))
        {
            POS = i;
            i = N;
        }
    }
    return POS;
}

/**
Fonction de chargement d'un tableau de structure � partir
d'un fichier text
*/

void load_liste_produit_from_file(int *nb, Prod TABPROD[], char* file_path)
{
    /**D�claration des variable*/
    FILE* fp_read = NULL;
    int i = 0;
    char ligne[100];

    /**Ecriture strcutur�e*/
    fp_read= fopen(file_path,"r");/**r:read ; w: write ; a : append*/
    if(!fp_read)
    {
        printf("Erreur de l'ouverture du fichier");
        exit(-1);
    }
    while(fgets(ligne,100,fp_read))
    {
        sscanf(ligne,"%f|%s",&TABPROD[i].prix,TABPROD[i].nom);
        i++;
    }
    *nb=i;
    fclose(fp_read);
}
/**
Fonction d'enregistrement d'un tableau de structure dans un fichier text
*/
void save_liste_produit_into_file(int nb, Prod TABPROD[], char* file_path)
{
    FILE* fp_write = NULL;
    int i;
    fp_write= fopen(file_path,"w");/**r:read ; w: write ; a : append*/
    if(!fp_write)
    {
        printf("Erreur de creation du fichier");
        exit(-1);
    }

    for(i = 0; i<nb; i++)
    {
        fprintf(fp_write,"%3.5f|%s\n",TABPROD[i].prix,TABPROD[i].nom);
    }
    fclose(fp_write);
}

void load_liste_produit_from_DB(int* N,Prod tabProd[], char* nom_bd_to_load){
     
    mysql_options(mysql,MYSQL_READ_DEFAULT_GROUP,"option");
        if(mysql_real_connect(mysql,"localhost" ,"root","root",nom_bd_to_load,0,NULL,0))
    //    if(mysql_real_connect(&mysql,"192.168.43.208","root","","tp1_cinema",0,NULL,0))
        {
            //Requ�te qui s�lectionne tout dans ma table scores
            mysql_query(con, "SELECT * FROM Produit ORDER BY nom");
            //D�claration des pointeurs de structure
            MYSQL_RES *result = NULL;
            MYSQL_ROW row;
            unsigned int i = 0;
            unsigned int num_champs = 0;
            int j =1;
            //On met le jeu de r�sultat dans le pointeur result
            result = mysql_use_result(mysql);
            //On r�cup�re le nombre de champs
            num_champs = mysql_num_fields(result);
            //on stock les valeurs de la ligne choisie
            while ((row = mysql_fetch_row(result)))
            {
                //On d�clare un pointeur long non sign� pour y stocker la taille des valeurs
                unsigned long *lengths;
                //On stocke ces tailles dans le pointeur
                lengths = mysql_fetch_lengths(result);

                //On fait une boucle pour avoir la valeur de chaque champs
                strcpy(tabProd[j-1].nom, row[2]);    //la trois�me colonne de l'uplet
                tabProd[j-1].prix = (float)atof(row[3]); //la quatri�me colonne de l'uplet
                printf("** Tab : prix %f nom %s ",tabProd[j-1].prix, tabProd[j-1].nom);

                //On affiche la position du produit
                printf("%ld- ", j);
                //OPTIONNEL JUSTE UN AFFICHAGE
                for(i = 1; i < num_champs; i++)
                {
                    //On ecrit toutes les valeurs
                    printf("%.*s ", (int) lengths[i], row[i] ? row[i] : "NULL");
                }
                printf("\n");
                //On incr�mente la position du joueur
                j++;
            }
            *N=j-1;
            //Lib�ration du jeu de r�sultat
            mysql_free_result(result);
        }
        else
        {
            printf("Une erreur s'est produite lors de la connexion � la BDD: %s", mysql_error(&mysql));
        }


}

// Sauver la liste des produits dans la DB.
void save_liste_produit_into_DB(int N,Prod tabProd[],char* nom_bd_to_save, MYSQL *con) {
    
    int i; // Variable i pour la boucle
    char query_line[100]; // Variable qui va parcourir les lignes du tableau
    unsigned int num_champs = 0; // Variable utilis�e pour le nombre de champs
    MYSQL_RES *result = NULL; // Pointeur de structure
    MYSQL_ROW row; // Pointeur de row de la DB

    // �tape de connexion � la DB
    mysql_options(mysql,MYSQL_READ_DEFAULT_GROUP,"option");
    
    if(mysql_real_connect(con,"localhost" ,"root","root",nom_bd_to_save,0,NULL,0) == NULL) {
        finish_with_error(con); // Fonction qui �vite les r�p�titions non-n�c�ssaires (selon http://zetcode.com/db/mysqlc/ - Lien donn� en cours)
        
        for (i = 0; i < N; i++) {
            printf(query_line, "INSERT INTO Produit(nom='%s',prix='%f');", tabProd[i].nom,tabProd[i].prix); // Insertion des donn�es dans la DB via langage SQL. Ici on insert le nom et le prix d'un nouveau produit, dans la table Produit de notre DB.
            }
            finish_with_error(con);
        
            MYSQL_RES *result = mysql_store_result(result); // Stockage des R�sultats dans la variable result
           
           if (result == NULL) {
               finish_with_error(result); // On v�rifie que la variable result ne soit pas vide avec la fonction finish_with_error vue sur http://zetcode.com/db/mysqlc/ - Lien donn� en cours
           }

           int num_champs = mysql_num_fields(result); // Stockage du resultat
           
           MYSQL_ROW row;
           
           while ((row = mysql_fetch_row(result))) { // Boucle pour la position du Produit dans la DB
               
               for (i = 0; i < num_champs; i++) {
                   printf("%s", row[i] ? row[i] : "NULL");
               }
               
               printf("\n");
           }
       
        mysql_free_result(result); // Lib�ration du R�sultat
        mysql_close(con); // Fermer la connexion � la DB
    }
    else {
        printf("Erreur lors de la connexion � la DB");
        finish_with_error(con);
    }
}


// CODE SQL UTILIS� POUR LA CR�ATION DE LA DB :

// CREATE DATABSE IF NOT EXIST 'bd_gestion_stock' (Ajout� manuellement en plus du coude fourni ci-dessous)

// CREATE TABLE IF NOT EXISTS `Adresse` (
//  `id` varchar(100)  NOT NULL,
//  `Fournisseur_id` varchar(100) NOT NULL,
//  `num_rue` VARCHAR(50) DEFAULT NULL,
//  `nom_rue` VARCHAR(50) DEFAULT NULL,
//  `ville` VARCHAR(50) DEFAULT NULL,
//  `code_postal` INTEGER UNSIGNED NULL,
//  `pays` VARCHAR(50) DEFAULT NULL,
//PRIMARY KEY (`id`),
//  KEY `Adresse_index_Fournisseur` (`Fournisseur_id`)
//) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des adresse';
//
//
//CREATE TABLE Fournisseur (
//  `id` varchar(100)  NOT NULL,
//  `nom_rue` VARCHAR(50) DEFAULT NULL,
//  `telephone` INTEGER UNSIGNED NULL,
//    PRIMARY KEY (`id`)
//)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des fournisseur';
//
//CREATE TABLE Produit (
//  `id` varchar(100)  NOT NULL,
//  `Fournisseur_id` VARCHAR(100) NOT NULL,
//  `nom` VARCHAR(50) DEFAULT NULL,
//  `prix` INTEGER UNSIGNED NULL,
//  `qtestq` FLOAT NULL,
//  `date_limite` DATE NULL,
//      PRIMARY KEY (`id`),
//  KEY `Produit_index_Fournisseur` (`Fournisseur_id`)
//) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table des produit';
//
//ALTER TABLE `Produit`
//  ADD CONSTRAINT `Produit_FKIndex1` FOREIGN KEY (`Fournisseur_id`) REFERENCES `Fournisseur` (`id`) ON UPDATE CASCADE;
//
//ALTER TABLE `Adresse`
//  ADD CONSTRAINT `Adresse_FKIndex1` FOREIGN KEY (`Fournisseur_id`) REFERENCES `Fournisseur` (`id`) ON UPDATE CASCADE;
//

